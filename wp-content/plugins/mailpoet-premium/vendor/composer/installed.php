<?php

if (!defined('ABSPATH')) exit;

 return array(
    'root' => array(
        'pretty_version' => 'dev-master',
        'version' => 'dev-master',
        'type' => 'library',
        'install_path' => __DIR__ . '/../../',
        'aliases' => array(),
        'reference' => '0d7478d0668b2fbc75eab39ab9b868e000242df3',
        'name' => '__root__',
        'dev' => false,
    ),
    'versions' => array(
        '__root__' => array(
            'pretty_version' => 'dev-master',
            'version' => 'dev-master',
            'type' => 'library',
            'install_path' => __DIR__ . '/../../',
            'aliases' => array(),
            'reference' => '0d7478d0668b2fbc75eab39ab9b868e000242df3',
            'dev_requirement' => false,
        ),
    ),
);
